import {Component, OnInit, ViewChild} from '@angular/core';
import {MmrService} from '../../service/mmr.service';
import {RankService} from '../../service/rank.service';
import {RainbowSixRank} from '../../service/entities/rainbow-six-rank';
import {MmrChannel} from '../../service/entities/mmr-channel';
import {UplayAccount} from '../../service/entities/uplay-account';
import {Client} from '../../service/entities/client';
import {SnapshotFetchResultSource} from '../../service/entities/snapshot-fetch-result-source';
import {RxStompService} from '@stomp/ng2-stompjs';
import {Message} from '@stomp/stompjs';
import {PlayerOverviewComponent} from '../../components/player-overview/player-overview.component';
import {InstanceInfoService} from '../../service/instance-info.service';
import {MmrCompareService} from '../../service/mmr-compare.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-mmr-overview',
  templateUrl: './teamspeak-channel-overview.component.html',
  styleUrls: ['./teamspeak-channel-overview.component.scss']
})
export class TeamspeakChannelOverviewComponent implements OnInit {

  @ViewChild(PlayerOverviewComponent) playerOverviewChild: PlayerOverviewComponent;

  minRankedClearanceLevel: number;
  maxMmrGap: number;
  mmrChannel: MmrChannel[] = null;

  // Keep a original copy of the data received by the API to check for changes
  mmrChannelOriginalData: string = null;
  interval;
  // The account which the user selected to compare MMR
  selectedAccountMMRDCalculation: UplayAccount = null;
  ubisoftAvatarUrlPrefix = 'https://ubisoft-avatars.akamaized.net/';
  ubisoftAvatarUrlSuffix = '/default_256_256.png';
  public readonly mmrGapWarningPercentage = 0.10;
  private readonly gamesLeftMaxPercentage = 10;

  constructor(private mmrService: MmrService,
              public rankService: RankService,
              public rxStompService: RxStompService,
              public instanceInfoService: InstanceInfoService,
              public mmrCompareService: MmrCompareService,
              public spinner: NgxSpinnerService) {
    this.rxStompService.watch('/topic/channel_update').subscribe((message: Message) => {
      console.log('Channel update requested via websocket: ' + message.body);
      this.loadMmrOverview();
    });
  }

  private static sortAccountByRankedMatches(uplayAccounts: UplayAccount[]) {
    uplayAccounts.sort((n1, n2) => {
      if (n1.rankedMatchCount < n2.rankedMatchCount) {
        return 1;
      }
      if (n1.rankedMatchCount > n2.rankedMatchCount) {
        return -1;
      }
      return 0;
    });
  }

  private static sortClientsByMainAccountMmr(clients: Client[]) {
    clients.sort((n1, n2) => {
      const n1MainMmr = this.getClientMainAccountMmr(n1);
      const n2MainMmr = this.getClientMainAccountMmr(n2);

      if (n1MainMmr < n2MainMmr) {
        return 1;
      }
      if (n1MainMmr > n2MainMmr) {
        return -1;
      }
      return 0;
    });
  }

  private static getClientMainAccountMmr(client: Client) {
    for (const ua of client.uplayAccounts) {
      if (ua._isMainAccount) {
        return ua.mmr;
      }
    }
    return -1;
  }

  ngOnInit() {
    setTimeout(() => {
      this.spinner.show('spinner-load-channel-overview', {
        type: 'line-scale-party',
        bdColor: 'rgba(127,127,127,0.56)',
        fullScreen: false
      });
    }, 0);
    this.loadMmrOverview();
  }

  loadMmrOverview() {
    this.mmrService.getMmrOverview()
      .subscribe((data: any) => {

        this.maxMmrGap = this.instanceInfoService.instanceDetails.maxMmrGapRanked;
        this.minRankedClearanceLevel = this.instanceInfoService.instanceDetails.minRankedClearanceLevel;
        // Create a string of the data so we can compare that against new data
        // and monitor for changes
        const newData = JSON.stringify(data['mmrChannel']);

        // Only update our cache when there is a change in data
        // This will prevent unnecessary updates in the view.
        if (!(this.mmrChannelOriginalData === null) && newData === this.mmrChannelOriginalData) {
          // nothing to refresh
        } else {
          // Update data only when it is different from the cached version.
          console.log('Data change detected, refreshing ...');
          this.mmrChannelOriginalData = newData;
          this.mmrChannel = data['mmrChannel'];
          this.calculate();
        }
        this.spinner.hide('spinner-load-channel-overview');
      });
  }

  setCalculateDifferenceUser(uplayAccount: UplayAccount) {
    this.selectedAccountMMRDCalculation = uplayAccount;
    this.calculateMmrDifferenceByUplayAccount();
  }

  calculateMmrDifferenceByUplayAccount() {
    const resetMmrCompare = !this.updateSelectedMMRCompareAccount();

    for (const channel of this.mmrChannel) {
      for (const cl of channel.clients) {
        for (const uplayAccount of cl.uplayAccounts) {
          if (this.selectedAccountMMRDCalculation !== null && !resetMmrCompare) {
            uplayAccount._mmrDifference = uplayAccount.mmr - this.selectedAccountMMRDCalculation.mmr;
          } else {
            uplayAccount._mmrDifference = null;
          }
        }
      }
    }
    if (resetMmrCompare === true) {
      this.selectedAccountMMRDCalculation = null;
    }
  }

  calculateMmrAverage() {
    for (const channel of this.mmrChannel) {
      let totalMmrInChannel = 0;
      let validClients = 0;
      for (const cl of channel.clients) {
        for (const uplayAccount of cl.uplayAccounts) {
          if (uplayAccount._isMainAccount === false) {
            continue; // Skip non main accounts
          }
          totalMmrInChannel += uplayAccount.mmr;
          validClients++;
        }
      }
      channel._mmrAverage = totalMmrInChannel / validClients;
    }
  }

  getRankForMmr(mmr: number): RainbowSixRank {
    return this.rankService.getRankForMmr(mmr);
  }

  getRankNameForMmr(mmr: number): string {
    const rank = this.getRankForMmr(mmr);
    if (rank == null) {
      return 'unknown';
    }
    return rank.aliasName;
  }

  computeMinimumClearanceLevel() {
    for (const channel of this.mmrChannel) {
      let unsatisfiedMainAccountClearanceLevelInChannel = false;
      for (const ocl of channel.clients) {
        ocl._anyAccountWithMinimalClearanceLevel = false;
        for (const uplayAccount of ocl.uplayAccounts) {
          uplayAccount._hasMinimumClearanceLevel = this.hasMinimumClearanceLevelForRanked(uplayAccount);
          if (uplayAccount._hasMinimumClearanceLevel === false) {
            if (uplayAccount._isMainAccount) {
              unsatisfiedMainAccountClearanceLevelInChannel = true;
            }
          } else {
            ocl._anyAccountWithMinimalClearanceLevel = true;
          }
        }
      }
      // If we have clients with unsatisfied clearance level, set a flag on the channel
      channel._hasClientsWithUnsatisfiedClearanceLevel = unsatisfiedMainAccountClearanceLevelInChannel;
    }
  }

  hasMinimumClearanceLevelForRanked(uplayAccount: UplayAccount) {
    return uplayAccount !== null ? uplayAccount.clearanceLevel >= this.minRankedClearanceLevel : false;
  }

  /**
   * Does this uplayAccount have a rank
   */
  isAccountRanked(uplayAccount: UplayAccount): boolean {
    if (uplayAccount._hasMinimumClearanceLevel === false) {
      return false;
    }
    if (uplayAccount.uplayUuid === null) {
      return false;
    }
    return uplayAccount.mmr !== 0 && uplayAccount.hasRank;
  }

  hasAnyRankedAccount(uplayAccounts: UplayAccount[]) {
    for (const account of uplayAccounts) {
      if (this.isAccountRanked(account)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Checks if this accounts exceeds the allowed percentage of left games.
   *
   * @param uplayAccount account to check
   */
  isLeaver(uplayAccount: UplayAccount): number {
    if (!this.isAccountRanked(uplayAccount)) {
      // Don't count un-ranked accounts since leaving only one game would be enough
      // to mark this person as leaver
      return 0;
    }
    if (uplayAccount.rankedMatchAbandons === 0) {
      // Finish here since it will be 0%
      return 0;
    }

    const matchCount = uplayAccount.rankedMatchWins + uplayAccount.rankedMatchLosses + uplayAccount.rankedMatchAbandons;

    return (uplayAccount.rankedMatchAbandons / matchCount) * 100;
  }

  getAccountWithMostMatches(uplayAccounts: UplayAccount[]): UplayAccount {
    TeamspeakChannelOverviewComponent.sortAccountByRankedMatches(uplayAccounts);
    if (uplayAccounts.length === 0) {
      return null;
    }
    return uplayAccounts[0];
  }

  amountOfClientWithMmrOfChannel(channel: MmrChannel): number {
    let accountsWithMmr = 0;
    for (const c of channel.clients) {
      for (const uplayAccount of c.uplayAccounts) {
        if (uplayAccount.mmr > 1) {
          accountsWithMmr++;
        }
      }
    }
    return accountsWithMmr;
  }

  canShowSummaryOfChannel(channel: MmrChannel): boolean {
    return channel.clients.length > 1 && this.amountOfClientWithMmrOfChannel(channel) > 1;
  }

  /**
   * Resolve avatar url for ubisoft CND by uplayAccount
   */
  getUbisoftAvatarForUuid(uplayAccount: UplayAccount) {
    return this.ubisoftAvatarUrlPrefix + uplayAccount.uplayUuid + this.ubisoftAvatarUrlSuffix;
  }

  hasResolveError(uplayAccount: UplayAccount) {
    return uplayAccount._resultSourceType === SnapshotFetchResultSource.API_FETCH_ERROR_USING_LATEST_SNAPSHOT;
  }

  public doMmrCompare() {
    console.log(this.mmrCompareService.selectedAccounts);
  }

  private calculate() {
    this.sortAccountsAndDefineMain();
    this.calculateMmrAverage();
    this.computeMinimumClearanceLevel();
    this.calculateMmrDifferenceByUplayAccount();
    this.calculateChannelMainAccountMmrGap();
    this.calculateLeavingPercentage();
    this.processSnapshotFetchResultSource();
    this.validateMmrCompareAccounts();
    this.refreshOpenUserModal();
  }

  /**
   * When a user is selected and the data is changed in the background
   * we need to make sure that the cache / "selected" copy of the user is updated,
   * when the data (channelData) changes.
   * So we loop trough all our channels -> Clients -> UPlay accounts and search for the selected entity.
   * When found we update the entity in our cache and return true.
   * When we can't find the selected entity we just return false.
   *
   * The reason for this is that otherwise the calculation data (that has been updated) would lead to some sort
   * of "session difference" (MMR changes while the session was open).
   */
  private updateSelectedMMRCompareAccount() {
    if (!this.selectedAccountMMRDCalculation) {
      return false;
    }
    for (const channel of this.mmrChannel) {
      for (const cl of channel.clients) {
        for (const ua of cl.uplayAccounts) {
          if (ua.uplayUuid === this.selectedAccountMMRDCalculation.uplayUuid) {
            this.selectedAccountMMRDCalculation = ua;
            return true;
          }
        }
      }
    }
    console.log('Unable to find cached MMR compare account.');
    return false;
  }

  private calculateChannelMainAccountMmrGap() {
    for (const ch of this.mmrChannel) {
      let minMmr = 0;
      let maxMmr = 0;
      for (const client of ch.clients) {
        for (const uplayAccount of client.uplayAccounts) {
          if (uplayAccount._isMainAccount === false) {
            continue;
          }
          if (minMmr === 0 || uplayAccount.mmr < minMmr) {
            minMmr = uplayAccount.mmr;
          }
          if (maxMmr === 0 || uplayAccount.mmr > maxMmr) {
            maxMmr = uplayAccount.mmr;
          }
        }
      }
      ch._calculatedMainAccountMmrGap = maxMmr - minMmr;
      ch._calculatedMmrGapExceeded = ch._calculatedMainAccountMmrGap > this.maxMmrGap;

      ch._calculatedMmrGapWarning = ch._calculatedMainAccountMmrGap > (this.maxMmrGap * (1.00 - this.mmrGapWarningPercentage));
    }
  }

  private validateMmrCompareAccounts() {
    const allAccounts: UplayAccount[] = [];
    for (const ch of this.mmrChannel) {
      for (const c of ch.clients) {
        for (const acc of c.uplayAccounts) {
          allAccounts.push(acc);
        }
      }
    }
    this.mmrCompareService.validateAccounts(allAccounts);
  }

  private calculateLeavingPercentage() {
    for (const channel of this.mmrChannel) {
      let channelHasLeaver = false;
      for (const cl of channel.clients) {
        let hasLeaver = false;
        let maxLeaverPercentage = 0;
        for (const uplayAccount of cl.uplayAccounts) {
          const leavePercentage = this.isLeaver(uplayAccount);
          uplayAccount._leaverPercentage = leavePercentage;
          uplayAccount._isLeaver = leavePercentage >= this.gamesLeftMaxPercentage;
          if (maxLeaverPercentage < uplayAccount._leaverPercentage) {
            maxLeaverPercentage = uplayAccount._leaverPercentage;
          }
          if (uplayAccount._isLeaver) {
            hasLeaver = true;
            channelHasLeaver = true;
          }
        }
        cl._includesLeaverAccount = hasLeaver;
        cl._maxLeaverPercentage = maxLeaverPercentage;
      }
      channel._hasLeaver = channelHasLeaver;
    }
  }

  private processSnapshotFetchResultSource() {
    for (const channel of this.mmrChannel) {
      for (const cl of channel.clients) {
        cl._resolveErrors = 0;
        for (const uplayAccount of cl.uplayAccounts) {
          uplayAccount._resultSourceType = (SnapshotFetchResultSource as any)[uplayAccount.resultSource];
          if (uplayAccount._resultSourceType === SnapshotFetchResultSource.API_FETCH_ERROR_USING_LATEST_SNAPSHOT) {
            cl._resolveErrors++;
          }
          uplayAccount._recentlyUpdated = uplayAccount._resultSourceType === SnapshotFetchResultSource.SNAPSHOT_CREATED;
        }
      }
    }
  }

  /**
   * Whenever data is updated, we check if a modal with the current MMR compare user is opened.
   * If this is the case we refresh the data in the modal.
   */
  private refreshOpenUserModal() {
    for (const channel of this.mmrChannel) {
      for (const cl of channel.clients) {
        for (const uplayAccount of cl.uplayAccounts) {
          if (uplayAccount._recentlyUpdated) {
            this.refreshModalWhenOpenedWithCurrentPlayer(uplayAccount);
          }
        }
      }
    }
  }

  private refreshModalWhenOpenedWithCurrentPlayer(uplayAccount: UplayAccount) {
    if (this.playerOverviewChild.profile.uuid === uplayAccount.uplayUuid) {
      if (this.playerOverviewChild.isDisplayingAccount(uplayAccount.uplayUuid)) {
        // Modal is currently opened with selected player, updating ...
        this.playerOverviewChild.openModal('euw', uplayAccount.uplayUuid);
      }
    }
  }

  private sortAccountsAndDefineMain() {
    for (const ch of this.mmrChannel) {
      for (const client of ch.clients) {
        // Set main account to false for all
        for (const currentUplayAccount of client.uplayAccounts) {
          currentUplayAccount._isMainAccount = false;
        }
        TeamspeakChannelOverviewComponent.sortAccountByRankedMatches(client.uplayAccounts);
        const uplayAccount = this.getAccountWithMostMatches(client.uplayAccounts);
        uplayAccount._isMainAccount = true;
      }
      // Sort channel clients by main account mmr
      TeamspeakChannelOverviewComponent.sortClientsByMainAccountMmr(ch.clients);
    }
  }
}
