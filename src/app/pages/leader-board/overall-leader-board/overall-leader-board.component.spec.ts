import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OverallLeaderBoardComponent} from './overall-leader-board.component';

describe('OverallLeaderBoardComponent', () => {
  let component: OverallLeaderBoardComponent;
  let fixture: ComponentFixture<OverallLeaderBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OverallLeaderBoardComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverallLeaderBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
