import {Component, OnInit} from '@angular/core';
import {InstanceInfoService} from './service/instance-info.service';
import {ThemeService} from './service/theme.service';
import {RxStompService} from '@stomp/ng2-stompjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {


  private static connectionErrorMessageSuppressionDelay = 1500;
  /**
   * Used to suppress the "no connection" message for a short amount of time, before we show it to the user.
   * Otherwise the user would see this message after every page-load.
   */
  public initialErrorSuppressionActive = true;
  public serverVersion: string;

  constructor(public instanceInfoService: InstanceInfoService,
              public themeService: ThemeService,
              public rxStompService: RxStompService) {
  }

  public toggleDarkMode() {
    this.themeService.setDarkTheme(!this.themeService.isDarkModeActive());
  }

  ngOnInit(): void {
    this.themeService.bootstrap();

    setTimeout(() => {
      // Disable initial error supression after a few seconds.
      this.initialErrorSuppressionActive = false;
    }, AppComponent.connectionErrorMessageSuppressionDelay);
  }
}
