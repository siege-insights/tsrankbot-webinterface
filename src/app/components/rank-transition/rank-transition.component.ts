import {Component, Input, OnInit} from '@angular/core';
import {RainbowSixRank} from '../../service/entities/rainbow-six-rank';
import {RankService} from '../../service/rank.service';

@Component({
  selector: 'app-rank-transition',
  templateUrl: './rank-transition.component.html',
  styleUrls: ['./rank-transition.component.scss']
})
export class RankTransitionComponent implements OnInit {

  @Input()
  public mmr: number;

  @Input()
  public mmrBefore: number;

  public arrowUp = '⇗';
  public arrowDown = '⇘';


  public transitionOccurred = false;
  public positiveTransition;

  public currentRank: RainbowSixRank;
  public rankBefore: RainbowSixRank;

  constructor(private rankService: RankService) {
  }

  ngOnInit(): void {
    this.process();
  }

  private process() {
    this.currentRank = this.getRank(this.mmr);
    this.rankBefore = this.getRank(this.mmrBefore);
    if (this.rankBefore != null && this.currentRank.id !== this.rankBefore.id) {
      // Rank did change!
      this.transitionOccurred = true;
      this.positiveTransition = this.mmr > this.mmrBefore;
    }
  }

  private getRank(mmr: number): RainbowSixRank {
    return this.rankService.getRankForMmr(mmr);
  }

}
