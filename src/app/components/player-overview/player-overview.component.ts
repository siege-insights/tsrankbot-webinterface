import {Component, OnInit, ViewChild} from '@angular/core';
import {RankService} from '../../service/rank.service';
import {LastMatchSummary} from './last-match-summary';
import {PlayerDetailsService} from '../../service/player-details.service';
import {Rest} from '../../service/entities/statsdb/transport';
import {NgxSpinnerService} from 'ngx-spinner';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {ChartSeriesDataElement} from '../../service/entities/chart-series-data-element';
import {DayOnlyDatePipePipe} from '../../pipes/day-only-date-pipe';
import {DayHourMinutePipe} from '../../pipes/day-hour-minute-pipe';
import {ToastrService} from 'ngx-toastr';
import {Spinner} from 'ngx-spinner/lib/ngx-spinner.enum';
import DailyPlayerStatsTO = Rest.DailyPlayerStatsTO;
import MatchHistoryEntryTO = Rest.MatchHistoryEntryTO;
import ProfileOverviewTO = Rest.ProfileOverviewTO;
import KDHistoryTO = Rest.KDHistoryTO;
import RankByMmrTO = Rest.RankByMmrTO;
import RankedMmrHistoryTO = Rest.RankedMmrHistoryTO;
import MatchHistoryTO = Rest.MatchHistoryTO;
import KdarDailyHistoryTO = Rest.KdarDailyHistoryTO;

const SPINNER_NAME_DETAILS = 'spinner-load-details';
const SPINNER_NAME_DAILY_STATS = 'spinner-load-daily-stats';
const SPINNER_NAME_MATCH_HISTORY = 'spinner-load-match-history';
const SPINNER_NAME_NEXT_RANK = 'spinner-load-next-rank';
const SPINNER_NAME_KDR = 'spinner-load-kdr';
const SPINNER_NAME_KDAR_HISTORY = 'spinner-load-kdar_history';

const detailsSpinnerSettings: Spinner = {
  type: 'ball-clip-rotate-multiple',
  bdColor: 'rgba(127,127,127,0.56)',
  fullScreen: false
};

const subComponentSpinnerSettings: Spinner = {
  type: 'line-scale-party',
  bdColor: 'rgba(127,127,127,0.56)',
  fullScreen: false
};

@Component({
  selector: 'app-player-overview',
  templateUrl: './player-overview.component.html',
  styleUrls: ['./player-overview.component.scss']
})
export class PlayerOverviewComponent implements OnInit {

  @ViewChild('playerOverviewModalTemplate') template;

  public kdrWarn = 0.9;
  modalRef: BsModalRef;
  public profile: ProfileOverviewTO;
  public lastMatchSummary: LastMatchSummary;
  public dailyPlayerStats: DailyPlayerStatsTO;
  public matchHistoryStats: MatchHistoryTO;
  public kdrBucketStats: KDHistoryTO[];
  public rankByMmrStats: RankByMmrTO;
  public rankedMmrHistoryStats: RankedMmrHistoryTO;
  public kdarDailyHistory: KdarDailyHistoryTO;

  public _recentEloChange: number;
  public _matchesToLowerRank: number;
  public _matchesToLowerRankVariation: number;
  public _matchesToUpperRank: number;
  public _matchesToUpperRankVariation: number;

  public _matchHistoryPage = 0;

  public _pagePadding = 3;


  constructor(private modalService: BsModalService,
              public rankService: RankService,
              private playerDetailsService: PlayerDetailsService,
              private spinner: NgxSpinnerService,
              private _dayOnlyDatePipe: DayOnlyDatePipePipe,
              private _dateHourMinutePipe: DayHourMinutePipe,
              private toastr: ToastrService) {

  }

  public openModal(region: string, uuid: string) {
    setTimeout(() => {
      this.spinner.show(SPINNER_NAME_DETAILS, detailsSpinnerSettings);
    }, 0);
    this.playerDetailsService.getProfileOverview(region, uuid).subscribe(data => {
      this.profile = data;
      this.spinner.hide(SPINNER_NAME_DETAILS);
      this.loadLazyData(region, uuid);
      if (this.profile !== undefined && uuid === this.profile.uuid) {
        // New open command has same user as before loaded in modal
        if (this.modalRef !== undefined && this.modalService.getModalsCount() > 0) {
          // Modal is already opened, just refresh data ...
          console.log('Not opening a new modal since the existing one was is the same user.');
        } else {
          // spawn a new modal
          this.spawnNewModal();
        }
      } else {
        this.spawnNewModal();
      }
    }, error => {
      this.spinner.hide(SPINNER_NAME_DETAILS);
      this.toastr.error('This problem might be temporary, please retry in a few moments.',
        'Error loading player details.');
    });
  }


  public isDisplayingAccount(uuid: string): boolean {
    if (this.profile === undefined) {
      return false;
    }
    return this.profile.uuid === uuid && this.modalService.getModalsCount() >= 1;
  }

  public unload() {
    this.profile = undefined;
    this.modalRef.hide();
  }

  public refresh(): void {
    this.openModal(this.profile.region, this.profile.uuid);
  }

  public isRankedMatch(logEntry: MatchHistoryEntryTO): boolean {
    return (logEntry.matchesWonChange > 0 || logEntry.matchesLostChange > 0 || logEntry.killDeathRatio > 0)
      && (logEntry.mmrChange < 0 || logEntry.mmrChange > 0)
      && !logEntry.multipleMatches;
  }

  public isPlacementMatch(logEntry: MatchHistoryEntryTO): boolean {
    return (logEntry.matchesLostChange > 0 || logEntry.matchesWonChange > 0 || logEntry.killDeathRatio > 0)
      && (logEntry.mmrChange === 0)
      && !logEntry.multipleMatches
      && logEntry.mmrCurrent === 0;
  }

  public isRankedAdjustment(logEntry: MatchHistoryEntryTO): boolean {
    return logEntry.matchesWonChange === 0
      && logEntry.matchesLostChange === 0
      && logEntry.killsChange === 0
      && logEntry.deathsChange === 0
      && logEntry.multipleMatches === false
      && (logEntry.mmrChange > 0 || logEntry.mmrChange < 0);
  }

  public isAbandon(logEntry: MatchHistoryEntryTO): boolean {
    return (this.isRankedMatch(logEntry) || this.isPlacementMatch(logEntry)) && logEntry.matchesWonChange === 0
      && logEntry.matchesLostChange === 0
      && logEntry.mmrChange < 0;
  }

  public calculateLastMatchesSummary() {
    const lastMatchSummary = new LastMatchSummary();
    for (const entry of this.matchHistoryStats.entries) {
      if (entry.multipleMatches) {
        continue; // Skip since we don't want to count in multiple matches...
      }
      if (!this.isRankedMatch(entry) || this.isRankedAdjustment(entry)) {
        continue; // Ignore non-ranked and adjustments
      }
      lastMatchSummary.countedMatches++;
      lastMatchSummary.won += entry.matchesWonChange;
      lastMatchSummary.lost += entry.matchesLostChange;
      lastMatchSummary.kills += entry.killsChange;
      lastMatchSummary.deaths += entry.deathsChange;
      lastMatchSummary.donuts += entry.killsChange === 0 ? 1 : 0;
      lastMatchSummary.doubleDigits += entry.killsChange >= 10 ? 1 : 0;
      lastMatchSummary.flawless += entry.deathsChange === 0 ? 1 : 0;
    }
    // IF deaths is zero set KD to zero
    if (lastMatchSummary.deaths === 0) {
      lastMatchSummary.kdr = 0;
    } else {
      lastMatchSummary.kdr = lastMatchSummary.kills / lastMatchSummary.deaths;
    }
    this.lastMatchSummary = lastMatchSummary;
  }

  public leaverPercentage(profile: ProfileOverviewTO): number {
    return profile.abandons / (profile.wins + profile.losses + profile.abandons) * 100;
  }

  public buildKillData(): ChartSeriesDataElement[] {
    const killData: ChartSeriesDataElement[] = [];
    for (const entry of this.matchHistoryStats.entries) {
      const newEntry = new ChartSeriesDataElement();
      newEntry.name = entry.snapshotCreatedDate.toString();
      newEntry.value = entry.killsChange;
      killData.push(newEntry);
    }
    return killData.reverse();
  }

  public buildAssistsData(): ChartSeriesDataElement[] {
    const assistsData: ChartSeriesDataElement[] = [];
    for (const entry of this.matchHistoryStats.entries) {
      const newEntry = new ChartSeriesDataElement();
      newEntry.name = entry.snapshotCreatedDate.toString();
      newEntry.value = entry.killAssistsChange;
      assistsData.push(newEntry);
    }
    return assistsData.reverse();
  }

  public buildDeathData(): ChartSeriesDataElement[] {
    const deathData: ChartSeriesDataElement[] = [];
    for (const entry of this.matchHistoryStats.entries) {
      const newEntry = new ChartSeriesDataElement();
      newEntry.name = entry.snapshotCreatedDate.toString();
      newEntry.value = entry.deathsChange;
      deathData.push(newEntry);
    }
    return deathData.reverse();
  }

  public buildKdrData(): ChartSeriesDataElement[] {
    const deathData: ChartSeriesDataElement[] = [];
    for (const entry of this.matchHistoryStats.entries) {
      const newEntry = new ChartSeriesDataElement();
      newEntry.name = entry.snapshotCreatedDate.toString();
      newEntry.value = entry.killDeathRatio;
      deathData.push(newEntry);
    }
    return deathData.reverse();
  }

  public buildBucketKdrData(): ChartSeriesDataElement[] {
    const entries: ChartSeriesDataElement[] = [];
    for (const entry of this.kdrBucketStats) {
      const newEntry = new ChartSeriesDataElement();
      newEntry.name = entry.timeBlock.toString();
      newEntry.value = entry.kdr;
      newEntry.min = entry.minKdr;
      newEntry.max = entry.maxKdr;
      entries.push(newEntry);
    }
    return entries.reverse();
  }

  public buildRankedMmrHistoryData(): ChartSeriesDataElement[] {
    const entries: ChartSeriesDataElement[] = [];
    for (const entry of this.rankedMmrHistoryStats.entries) {
      const newEntry = new ChartSeriesDataElement();
      newEntry.name = entry.snapshotDate.toString();
      newEntry.value = entry.mmr;
      entries.push(newEntry);
    }
    return entries.reverse();
  }

  /**
   * Checks if a stitch transition did occur between two dates
   *
   * @param currentRaw Date of the current dataset
   * @param beforeRaw Date of the row before
   * @param stitchDateRaw Stitch date configured by the server
   */
  public isStitchTransition(currentRaw: Date, beforeRaw: Date, stitchDateRaw: Date) {
    const current = new Date(currentRaw);
    const before = new Date(beforeRaw);
    const stitchDate = new Date(stitchDateRaw);
    // Is the current day the same as from the dataset before and after our stitch hour?
    if (current.getUTCDay() === before.getUTCDay() && current.getHours() >= stitchDate.getHours()) {
      return false;
    }
    // Is the current day +1 to the before day? If so is the stitch date not reached yet?
    if (current.getUTCDay() + 1 === before.getUTCDay()
      && current.getHours() < stitchDate.getHours()
      || before.getHours() < stitchDate.getHours()) {
      return false;
    }
    return true;
  }

  public loadMatchHistoryPage(page: number): void {
    if (page < 0 && this._matchHistoryPage < 0) {
      this._matchHistoryPage = 0;
    } else {
      this._matchHistoryPage = page;
    }
    this.loadMatchHistory(this.profile.region, this.profile.uuid);
  }

  // entry.snapshotCreatedDate.getUTCDate() != matchHistoryStats[i -1].snapshotCreatedDate.getUTCDate()
  ngOnInit(): void {
  }

  private spawnNewModal() {
    if (this.modalService.getModalsCount() > 0) {
      this.unload();
    }
    this.modalRef = this.modalService.show(this.template, {
      class: 'modal-lg'
    });
  }

  private loadLazyData(region: string, uuid: string) {
    this.lastMatchSummary = undefined;
    this._recentEloChange = undefined;
    this._matchesToLowerRank = undefined;
    this._matchesToUpperRank = undefined;
    this._matchHistoryPage = 0;

    this.loadDailyStats(region, uuid);
    this.loadMatchHistory(region, uuid);
    this.loadKdrBucketStats(region, uuid);
    this.loadRankByMmr(uuid);
    this.loadRankedMmrHistory(region, uuid);
    this.loadKdarDailyHistory(region, uuid);
  }

  private loadDailyStats(region: string, uuid: string) {
    this.dailyPlayerStats = undefined;
    setTimeout(() => {
      this.spinner.show(SPINNER_NAME_DAILY_STATS, subComponentSpinnerSettings);
    }, 0);
    this.playerDetailsService.getDailyStats(region, uuid).subscribe(dailyStats => {
        this.spinner.hide(SPINNER_NAME_DAILY_STATS);
        this.dailyPlayerStats = dailyStats;
      },
      error => {
        this.spinner.hide(SPINNER_NAME_DAILY_STATS);
        this.toastr.error('This problem might be temporary, please retry in a few moments.',
          'Error loading daily stats.');
      });
  }

  private loadMatchHistory(region: string, uuid: string) {
    if (this._matchHistoryPage < 0) {
      this._matchHistoryPage = 0;
    }
    this.matchHistoryStats = undefined;
    setTimeout(() => {
      this.spinner.show(SPINNER_NAME_MATCH_HISTORY, subComponentSpinnerSettings);
    }, 0);
    setTimeout(() => {
      this.spinner.show(SPINNER_NAME_NEXT_RANK, subComponentSpinnerSettings);
    }, 0);
    this.playerDetailsService.getMatchHistory(region, uuid, this._matchHistoryPage).subscribe(matchHistoryData => {
        this.spinner.hide(SPINNER_NAME_MATCH_HISTORY);
        this.matchHistoryStats = matchHistoryData;
        this.calculateLastMatchesSummary();
        // We need to figure out the users most recent ELO loss/gain ...
        let mostRecentEloChange;
        for (const match of matchHistoryData.entries) {
          if (match.multipleMatches === false && (match.mmrChange < 0 || match.mmrChange > 0)) {
            // Pick the first MMR change that is present and not made out of multiple matches
            mostRecentEloChange = match.mmrChange;
            break;
          }
        }
        this.calculateMatchesToNextRank(this.profile.mmr, mostRecentEloChange);
      },
      error => {
        this.spinner.hide(SPINNER_NAME_MATCH_HISTORY);
        this.toastr.error('This problem might be temporary, please retry in a few moments.',
          'Error loading match-history stats.');
      });
  }

  private loadKdrBucketStats(region: string, uuid: string) {
    this.kdrBucketStats = undefined;
    setTimeout(() => {
      this.spinner.show(SPINNER_NAME_KDR, subComponentSpinnerSettings);
    }, 0);
    this.playerDetailsService.getKdrHistory(region, uuid).subscribe(data => {
        this.spinner.hide(SPINNER_NAME_KDR);
        this.kdrBucketStats = data;
      },
      error => {
        this.spinner.hide(SPINNER_NAME_KDR);
        this.toastr.error('This problem might be temporary, please retry in a few moments.',
          'Error loading KDR-history stats.');
      });
  }

  private loadRankByMmr(uuid: string) {
    this.rankByMmrStats = undefined;
    this.playerDetailsService.getRankByMmr(uuid).subscribe(data => {
        this.rankByMmrStats = data;
      },
      error => {
        this.toastr.error('This problem might be temporary, please retry in a few moments.',
          'Error loading Rank-by-MMR-history stats.');
      }
    );
  }

  private loadRankedMmrHistory(region: string, uuid: string) {
    this.rankedMmrHistoryStats = undefined;
    this.playerDetailsService.getRankedMmrHistory(region, uuid).subscribe(data => {
        this.rankedMmrHistoryStats = data;
      },
      error => {
        this.toastr.error('This problem might be temporary, please retry in a few moments.',
          'Error loading MMR-history stats.');
      });
  }

  private loadKdarDailyHistory(region: string, uuid: string) {
    this.kdarDailyHistory = undefined;
    setTimeout(() => {
      this.spinner.show(SPINNER_NAME_KDAR_HISTORY, {
        type: 'line-scale-party',
        bdColor: 'rgba(127,127,127,0.56)',
        fullScreen: false
      });
    }, 0);
    this.playerDetailsService.getKdarDailyHistory(region, uuid).subscribe( data => {
      this.kdarDailyHistory = data;
        this.spinner.hide(SPINNER_NAME_KDAR_HISTORY);
    },
      error => {
      this.toastr.error('This problem might be temporary, please retry in a few moments.',
                   'Error loading KDAR-Daily-History stats.');
      this.spinner.hide(SPINNER_NAME_KDAR_HISTORY);
      });
  }

  private calculateMatchesToNextRank(mmr: number, eloChange: number): void {
    if (eloChange === 0) {
      console.log('Can\'t calculate required matches because eloChange is not set');
      return;
    }
    console.log('Using elo of: ' + eloChange + ' for matchesToNextRank calculation');
    this._recentEloChange = eloChange;
    this._matchesToLowerRank = this.rankService.numberOfMatchesToNextRank(mmr, this._recentEloChange, false);
    this._matchesToLowerRankVariation = this.rankService.convertVariationOffset(this._matchesToLowerRank);
    this._matchesToUpperRank = this.rankService.numberOfMatchesToNextRank(mmr, this._recentEloChange, true);
    this._matchesToUpperRankVariation = this.rankService.convertVariationOffset(this._matchesToUpperRank);
    this.spinner.hide(SPINNER_NAME_NEXT_RANK);
  }
}
