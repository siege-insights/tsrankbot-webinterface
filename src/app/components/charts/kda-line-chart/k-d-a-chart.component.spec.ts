import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {KDAChartComponent} from './k-d-a-chart.component';

describe('PlayerOverviewLineChartComponent', () => {
  let component: KDAChartComponent;
  let fixture: ComponentFixture<KDAChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KDAChartComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KDAChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
