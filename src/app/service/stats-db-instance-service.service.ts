import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Rest} from './entities/statsdb/transport';
import {environment} from '../../environments/environment';
import {ToastrService} from 'ngx-toastr';
import InstanceInfoTO = Rest.InstanceInfoTO;

@Injectable({
  providedIn: 'root'
})
export class StatsDbInstanceServiceService {


  public instanceInfo: InstanceInfoTO;

  private instanceInfoUrl = environment.statsDBApiUrl + 'instance/info';


  constructor(private http: HttpClient, private tostr: ToastrService) {
    this.getInstanceInfo().subscribe(data => {
        this.instanceInfo = data;
      },
      error => {
        this.tostr.error('Connection to the StatsDB backend failed.', 'Backend connection error');
      });
  }

  getInstanceInfo(): Observable<InstanceInfoTO> {
    return this.http.get<InstanceInfoTO>(this.instanceInfoUrl);
  }
}
