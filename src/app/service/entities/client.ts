import {UplayAccount} from './uplay-account';

export interface Client {
  teamspeakUuid: string;
  tsClientName: string;
  uplayAccounts: UplayAccount[];
  _anyAccountWithMinimalClearanceLevel: boolean;
  _includesLeaverAccount: boolean;
  _maxLeaverPercentage: number;
  _resolveErrors: number;
}
