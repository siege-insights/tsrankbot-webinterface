export interface TsClientSnapshotHistory {
  snapshotId: number;
  snapshotDate: number;
  mmr: number;
  mmrChange: number;
  kills: number;
  deaths: number;
  kdr: number;
  rankedMatchesWon: number;
  rankedMatchesLost: number;
  rankedMatchesAbandoned: number;
  multipleMatches: boolean;
}
