import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MmrService {

  apiUrl = environment.rankBotApiUrl + 'mmr/overview';

  constructor(private http: HttpClient) {
  }

  getMmrOverview() {
    return this.http.get(this.apiUrl);
  }
}
