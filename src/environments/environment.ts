// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // rankBotApiUrl: 'https://stats.siege-insights.net/api/', // Production version of RankBot Server
  rankBotApiUrl: '__DEPLOY_SITE_API_URL__', // Production version of RankBot Server
  // rankBotApiUrl: 'http://localhost:8082/', // local development RankBot Server
  // statsDBApiUrl: 'https://statsdb-api.siege-insights.net/', // Production StatsDB Server
  statsDBApiUrl: '__DEPLOY_STATSDB_API_URL__', // Production StatsDB Server
  // statsDBApiUrl: 'http://localhost:8081/open/', // local development StatsDB Server
  // websocketUrl: 'wss://stats.siege-insights.net/jsa-stomp-endpoint/', // Production websocket on RankBot Server
  websocketUrl: '__DEPLOY_WEBSOCKET_URL__', // Production websocket on RankBot Server
  // websocketUrl: 'ws://localhost:8082/jsa-stomp-endpoint/', // local development websocket on RankBot Server
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
