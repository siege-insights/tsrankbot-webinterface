/**
 * Used for complete local development
 */
export const environment = {
  production: false,
  rankBotApiUrl: 'http://localhost:8082/', // local development RankBot Server
  statsDBApiUrl: 'http://localhost:8081/open/', // local development StatsDB Server
  websocketUrl: 'ws://localhost:8082/jsa-stomp-endpoint/', // local development websocket on RankBot Server
};
