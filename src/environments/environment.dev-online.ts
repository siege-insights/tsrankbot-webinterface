/**
 * Used for development against the productive backends (online)
 * This is more intended to be used for reproducing bugs (which were observed in production)
 * and smaller fixes / features which do not require backend changes.
 */
export const environment = {
  production: false,
  rankBotApiUrl: 'https://stats.siege-insights.net/api/',
  statsDBApiUrl: 'https://statsdb-api.siege-insights.net/',
  websocketUrl: 'wss://stats.siege-insights.net//jsa-stomp-endpoint/'
};
