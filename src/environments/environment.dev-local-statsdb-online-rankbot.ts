/**
 * Used for development against the productive (online) tsrankbot, but local statsDB
 * This is more intended to be used for reproducing bugs (which were observed in production).
 */
export const environment = {
  production: false,
  rankBotApiUrl: 'https://stats.siege-insights.net/api/',
  statsDBApiUrl: 'http://localhost:8081/open/', // local development StatsDB Server
  websocketUrl: 'wss://stats.siege-insights.net//jsa-stomp-endpoint/'
};
