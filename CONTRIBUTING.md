# How to contribute
Whenever you wan't to contribute something to the project, feel free to submit a merge request.

Please check if an issue is already present before starting to work on something (would be bad if two people work on the same stuff).

If you want to fix an issue, please create an actual issue first and link it to the merge request.


If there is an issue, just comment that you are going to work on this and you will be assigned to that issue.
