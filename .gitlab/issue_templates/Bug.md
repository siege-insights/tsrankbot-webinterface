Whenever you report a bug, please fill out the following sections:

# Description of the issue
... What is going wrong?

## What is the expected behavior (how do you think should the program behave?)
...

## And what is the program actually doing?
...

## Ideas to fix (if you have a sugesstions on how to fix this issue, describe it here)
...

## Additional information (screenshots etc.)
 
